#Sheet9
#Aufgabe1
#parametric  test
#independent
#h0 <-mv1 > mv2
alpha <- 0.05


# a) Gau?-Test: 2 independent random variables with known variances
# Sample: Aufgabensammlung Lehn: Nr. 121
# example slide 8
sx <- 0.5; sy <- 0.6
x <- c(5.46,5.34,4.34,4.82,4.40,5.12,5.69,5.53,4.77,5.82)
y <- c(5.45,5.31,4.11,4.69,4.18,5.05,5.72,5.54,4.62,5.89,5.60,
       5.19,3.31,4.43,5.30,4.09)
nx <- length(x); ny <- length(y)
test.stat <- (mean(x)-mean(y))/sqrt(sx^2/nx + sy^2/ny)

# H0: mx <= my
# one sided: reject H0, if test.stat > 1-alpha quantil
quantil <- qnorm(1-alpha,0,1)
pvalue <- 1-pnorm(test.stat,0,1)
test.stat;quantil;pvalue 

#Aufgabe2
#h0: mv1  < mv 2
alpha <- 0.05
x <- c(7.06, 11.84, 9.28, 7.92, 13.5, 3.98, 3.82, 7.34, 8.7, 9.24, 4.86, 3.32,
        12.78, 12, 5.24, 11.4, 6.56, 9.04, 7.72, 9.26, 7.88, 8.6, 9.3, 8.42, 8.54)

y <- c(8.68, 6, 6.3, 10.24, 10.88, 5.36, 7.82, 4.7, 9.02, 9.78, 6.9, 5.8, 13.56,
        10.32, 13.3, 11.38, 7.94, 10.74, 13.68, 14.92, 7.42, 10.36, 10.54,
        5.22, 13.74, 12.98, 10.34, 10.02, 17.8, 13.04, 5.2, 9.4, 11.18, 12.68,
        12.36)

nu <- ( sd(x)^2/length(x) +sd(y)^2/length(y) )^2 / 
  ( (sd(x)^2/length(x))^2/(length(x)-1) + (sd(y)^2/length(y))^2/(length(y)-1) )
nu
t.test(x,y, alternative = "less", paired = FALSE, var.equal = FALSE,
       conf.level = 1-alpha)

# h0 is acepted

#Aufgabe 3


x <- c(16, 15, 11, 20, 19, 14, 13, 15, 14, 16)
y <- c(13, 13, 10, 18, 17, 11, 10, 15, 11, 16)
alpha <- 0.05
t.test(x,y,alternative="less",mu=0,paired=FALSE,var.equal=TRUE,
       conf.level=1-alpha)
#Aufgabe14
#import csv
# works fine
path <- "C:/Users/Simon/tmp/magnets_pain.csv"
#works fine
path <- "C:\\Users\\Simon\\tmp\\magnets_pain.csv"

path <- "D:\\uni\\statistik\\edditional_exercise_data\\sheet9\\magnets_pain.csv"
data<-read.csv2(path)


#b)
boxplot(data )

boxplot(data,names=colnames ( data[, c("Score_2")] ),main = "überschrift")

me <- mean(data$Score_2)
mi <- min(data$Score_2)
ma <- max(data$Score_2)
quantile(data$Score_2, p=0.25, type=1)
quantile(data$Score_2, p=0.25, type=1)
quantile(data$Score_2, p=0.75, type=1)

IQR(data$Score_2, type = 1)
sd(data$Score_2)

#c)

# ander aufgabe 5
alpha <- 0.1
x <- c(7.2, 4.1, 5.5, 4.5, 5.7, 3.8, 4.6, 6.0, 5.2, 5.4)
y <- c(5.3, 4.4, 5.0, 3.5, 3.9, 4.9, 5.6, 2.5, 4.0, 3.6)
var.test(x,y,alternative="two.sided",conf.level=1-alpha)

#b)

alpha <- 0.025
t.test(x,y,alternative="greater",mu=0,paired=FALSE,var.equal=TRUE,
       conf.level=1-alpha)
#Wir lehnen ab weril p value

#aufgabe 4
alpha <- 0.05
x <- c(102.4, 101.3, 97.6, 98.2, 102.3, 99.1, 97.8, 103.9, 101.6, 100.1)
y <- c(98.4, 101.7, 100.5, 99.3, 100.6, 99.6, 102.2, 101.1, 99.9, 101.0)
var.test(x,y,alternative="two.sided",
       conf.level=1-alpha)

# F und P sagen h0 annehmen





