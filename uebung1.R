library("tidyverse")
library("ggplot2")
#clean envimonent rm(list = ls())
#clean console strg + l
# Aufgabe 1
sum(52.3, 74.8, 3.17)
sqrt(144)
log10(200) * sin(pi/4)
x<-c(1,3,18,20,2)
cumsum(x)



sample(0:20, replace = TRUE)
# Generate 10 random numbers between 0 and 20 rounded to the nearest integer
random_numbers <- round(runif(10, min = 0, max = 20))

# Print the generated numbers
print(random_numbers)

# Aufgabe 2
x<-5
y<-10

z<-x*y
myvec<-c(x,y,z)
min(myvec)
max(myvec)
mean(myvec)
rm(myvec)

# Aufgabe 3
rainfall<-c(0.1, 0.5, 2.3, 1.1, 11.3, 14.7, 23.4, 15.7, 0, 0.9)
mean(rainfall)
sd(rainfall)
rain_cum<-cumsum(rainfall)#70.0
rainfall_max<-which.max(rainfall)#day 7
rainfall[rainfall_max]
#Take a subset of the rainfall data where rain is larger than 10.
vec_filter<-rainfall[rainfall > 10]
#What is mean rainfall for days where the rainfall was at least 5?
vec_filter<-rainfall[rainfall <= 5]
mean(vec_filter) #0.8166667
#Subset the vector where it is either exactly 0 or 1.1 and find the corresponding days.
which(rainfall == 0 | rainfall == 1.1) # 4 und 9
#Aufgabe 4
#The length of five cylinders are 2.5, 3.4, 4.8, 3.1, 1.7 and their diameters
#are 0.7, 0.4, 0.5, 0.5, 0.9.
cylinder_length<-c(2.5, 3.4, 4.8, 3.1, 1.7)
cylinder_diameter<-c(0.7, 0.4, 0.5, 0.5, 0.9)
cylinder_volumes<-c()
#Calculate the volumes of each cylinder and store it in a new vector. v=pi*r^2*h
for (i in 1:length(cylinder_length)) {
  cylinder_volumes[i]<-pi*(cylinder_diameter[i]/2)^2*cylinder_length[i]
}
#Assume the values are given in centimeter. Recalculate the volumes so that their units are cubic millimeter.
cylinder_volumes_cubicmillimeter <- cylinder_volumes*1000
#Aufgabe 5
x <- c(1,2,3,4,5)
y <- c(3,5,7,9)

#Find values that are contained in both x and y.
intersect(x,y)
#Find values that are in x but not y and vice versa.
setdiff(x,y)
setdiff(y,x)
#Construct a vector that contains all values contained in either x
#or y. Compare the result with c(x,y).
result<-union(x,y)
test<-c(x,y) #frage ob die elemente in den Mengen doppelt vorkommen sollen?
#Aufgabe 6
vec<-seq(from= 0, to = 18, by = 2)
a<-matrix(c(vec,as.integer(runif(70,0,10))),byrow=T, 8,10)
#Calculate the row means of this matrix (use rowMeans()) and the
#standard deviation across the row means.
rowMeans(a)
sd(rowMeans(a))

#Store the rows 2,3,..,8 in a other matrix and calculate the column
#means (use colMeans()). Use the command hist() to create a
#histogram of the column means.

new_matrix<-a[2:8, 1:10]
colMeans(new_matrix)
hist(colMeans(new_matrix))
#Aufgabe 7
#https://rstudio-pubs-static.s3.amazonaws.com/322396_ca6932a8cca04ee2b33d9cebdef8142b.html
str(mpg)
qplot(manufacturer, data=mpg, geom="bar", fill=manufacturer)
colnames(mpg)
#c https://tibble.tidyverse.org
tibble(str_mpg)

#Aufgabe 8
#(a)
parents<-list(father ="John", mother="Mary","ages of children" =c(4, 6, 10))
#(b)
children <-list("Bob", "Cate", "Susan")
#(c)
c(parents, children)
family<-list(parents, children)
result<-list(names=family[2][1],ages=family[[1]]["ages of children"][1])
family[[1]]["ages of children"][1]
family["ages of children"]



#Gegebene Listen
liste_1 <- list(vater="John", mutter="Mary", kinder_alter=c(4, 6, 10))
liste_2 <- list(kind_1_name="Bob", kind_2_name="Cate", kind_3_name="Susan")

#Neue Liste erstellen
liste_kinder <- list()

#Schleife, um Namen und Alter hinzuzufügen
for (i in 1:length(liste_2)) {
  kind_name <- liste_2[[paste0("kind_", i, "_name")]]
  kind_alter <- liste_1[["kinder_alter"]][i]
  liste_kinder[[i]] <- list(name = kind_name, alter = kind_alter)
}

#Ergebnisliste erstellen
list_concatinate_1 <- c(liste_1, liste_kinder)